import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ThreadList from './component/ThreadList/ThreadList'
class App extends Component {

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">challenge accepted</h1>
        </header>
        <div className="App-intro">
          <ThreadList/>
        </div>
      </div>
    );
  }
}

export default App;
